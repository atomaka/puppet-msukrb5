#
class msukrb5::php(
  $package_ensure      = 'present',
  $php_packages        = $msukrb5::php_packages,
  $php_pam_config_file = $msukrb5::php_pam_config_file,
) {
  package { $php_packages:
    ensure => $package_ensure
  }

  file { $php_pam_config_file:
    ensure    => present,
    source    => 'puppet:///modules/msukrb5/php',
  }
}
