#
class msukrb5(
  $packages            = $msukrb5::params::packages,
  $conf_file           = $msukrb5::params::conf_file,
  $password_file       = $msukrb5::params::password_file,
  $php_packages        = $msukrb5::params::php_packages,
  $php_pam_config_file = $msukrb5::params::php_pam_config_file,
  $test_servers        = $msukrb5::params::test_servers
) inherits msukrb5::params {
  class { 'msukrb5::install': } ->
  class { 'msukrb5::config': }
}
