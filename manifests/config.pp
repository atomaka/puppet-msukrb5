#
class msukrb5::config {
  file { $msukrb5::conf_file:
    ensure  => present,
    content => template('msukrb5/krb5.conf.erb'),
  }

  file { $msukrb5::password_file:
    ensure => present,
    source => 'puppet:///modules/msukrb5/common-password',
  }
}
