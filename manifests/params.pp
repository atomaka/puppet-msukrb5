#
class msukrb5::params {
  case $::osfamily {
    Debian: {
      $packages            = ['krb5-user', 'libpam-krb5']
      $conf_file           = '/etc/krb5.conf'
      $password_file       = '/etc/pam.d/common-password'
      $php_packages        = ['php5-auth-pam']
      $php_pam_config_file = '/etc/pam.d/php'
    }
    default: {
      fail("The ${module_name} module is not yet supported on ${::osfamily}
        systems")
    }
  }

  $test_servers = false
}
