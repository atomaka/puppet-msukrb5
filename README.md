# Description
Puppet module for installing Kerberos and the recommended configruation file for
MSU's implementation.

# Usage

```code
class { 'msukrb5': }
```
Installs Kerberos 5 packages and the krb5.conf recommended krb5.conf file.

```code
class { 'msukrb5::php': }
```
Installs the PHP pam auth package and configures pam to authenticate against
Kerberos for PHP.

# Limitations
Currently only available for Debian family distributions.